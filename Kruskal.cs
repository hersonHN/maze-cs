using System;
using System.Collections.Generic;
using System.Text;

namespace Kruskal {

    public class Point {
        public int x;
        public int y;

        public Point (int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    public class Edge : IComparable {
        #region Members

        private Vertex v1, v2;
        private int cost;
        private string name;

        #endregion

        #region Public Properties


        public Vertex V1 {
            get {
                return v1;
            }
        }

        public Vertex V2 {
            get {
                return v2;
            }
        }

        public int Cost {
            get {
                return cost;
            }
        }

        public string Name {
            get {
                return name;
            }
            set {
                this.name = value;
            }
        }

        public bool Check { get; set; }

        #endregion

        #region Constructor

        public Edge(Vertex v1, Vertex v2, int cost, string name) {
            this.v1 = v1;
            this.v2 = v2;
            this.cost = cost;
            this.name = name;
        } 

        #endregion

        #region IComparable Members

        public int CompareTo(object obj) {
            Edge e = (Edge)obj;
            return this.cost.CompareTo(e.cost);
        }

        #endregion
    }

    public class Vertex {
        #region Members

        private string name;

        #endregion

        #region Public Properties

        public string Name {
            get {
                return name;
            }
        }

        public int Rank { get; set; }

        public Vertex Root { get; set; }

        public Point Position { get; set; }


        #endregion

        #region Constructor

        public Vertex(string name) {
            this.name = name;
            this.Rank = 0;
            this.Root = this;
        }

        public Vertex(string name, Point position) {
            this.name = name;
            this.Rank = 0;
            this.Root = this;
            this.Position = position;
        } 

        #endregion

        #region Methods

        internal Vertex GetRoot() {
            if (this.Root != this) { // am I my own parent ? (am i the root ?)
                this.Root = this.Root.GetRoot();// No? then get my parent
            }

            return this.Root;
        }

        internal static void Join(Vertex root1, Vertex root2) {
            if (root2.Rank < root1.Rank) { // is the rank of Root2 less than that of Root1 ?
                root2.Root = root1;        //yes! then Root1 is the parent of Root2 (since it has the higher rank)
            } else  { // rank of Root2 is greater than or equal to that of Root1
                root1.Root = root2;//make Root2 the parent
                if (root1.Rank == root2.Rank) { // both ranks are equal ?
                    root2.Rank++;//increment Root2, we need to reach a single root for the whole tree
                }
            }
        }

        #endregion
    }

    public class Kruskal {
        #region Kruskal

        public List<Edge> Solve(List<Edge> graph, out int totalCost) {
            QuickSort(graph, 0, graph.Count - 1);
            List<Edge> solvedGraph = new List<Edge>();
            totalCost = 0;

            foreach (Edge ed in graph) {
                Vertex root1 = ed.V1.GetRoot();
                Vertex root2 = ed.V2.GetRoot();

                if (!root1.Name.Equals(root2.Name)) {

                    totalCost += ed.Cost;
                    Vertex.Join(root1, root2);
                    solvedGraph.Add(ed);
                }
            }

            return solvedGraph;
        } 

        #endregion

        #region Private Methods

        public void QuickSort(List<Edge> graph, int left, int right) {
            int i, j, x;
            i = left; j = right;
            x = graph[(left + right) / 2].Cost;

            do {
                while ((graph[i].Cost < x) && (i < right)) {
                    i++;
                }

                while ((x < graph[j].Cost) && (j > left)) {
                    j--;
                }

                if (i <= j) {
                    Edge y = graph[i];
                    graph[i] = graph[j];
                    graph[j] = y;
                    i++;
                    j--;
                }
            } while (i <= j);

            if (left < j) {
                QuickSort(graph, left, j);
            }

            if (i < right) {
                QuickSort(graph, i, right);
            }
        } 

        #endregion
    }

}


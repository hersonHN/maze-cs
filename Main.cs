using System;
using System.Collections.Generic;
using Kruskal;

namespace Maze {
    class Test {
        public static void Main (string[] args) {
            Console.WriteLine(new Maze(10, 10));
        }

        public static void TestGraph (string[] args) {

            Vertex a, b, c, d, e, f, g, h, i;
            a = new Vertex("A");
            b = new Vertex("B");
            c = new Vertex("C");
            d = new Vertex("D");
            e = new Vertex("E");
            f = new Vertex("F");
            g = new Vertex("G");
            h = new Vertex("H");
            i = new Vertex("I");

            Edge ab, bc, ac, ad, cd, ce, df, ef, ei, eg, fg, dh;
            ab = new Edge(a, b, 2, "ab");
            bc = new Edge(b, c, 5, "bc");
            ac = new Edge(a, c, 4, "ac");
            ad = new Edge(a, d, 6, "ad");
            cd = new Edge(c, d, 1, "cd");
            ce = new Edge(c, e, 2, "ce");
            df = new Edge(d, f, 3, "df");
            ef = new Edge(e, f, 1, "ef");
            ei = new Edge(e, i, 3, "ei");
            eg = new Edge(e, g, 5, "eg");
            fg = new Edge(f, g, 4, "fg");
            dh = new Edge(d, h, 4, "dh");

            List<Edge> edgeList = new List<Edge>
                { ab, bc, ac, ad, cd, ce, df, ef, ei, eg, fg, dh };

            int total_cost;
            Kruskal.Kruskal kruskal = new Kruskal.Kruskal();
            List<Edge> solution = kruskal.Solve(edgeList, out total_cost);

            foreach (Edge edge in solution) {
                Console.WriteLine("Edge {0}: " + edge.Cost, edge.Name);
            }
            Console.WriteLine("Total cost: {0}", total_cost);
        }
    }
}



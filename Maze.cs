using System;
using System.Collections.Generic;
using Kruskal;

namespace Maze {
    public class Maze {
        private Cell[,] cells;
        private int rows, cols;
        private List<Edge> graph;

        public Maze(int rows, int cols) {
            this.rows = rows;
            this.cols = cols;
            this.createCells();
            this.createEdges();
            this.createGraph();
            this.markBorders();
            this.joinFreeBorders();
        }

        private void createCells() {
            int rows = this.rows;
            int cols = this.cols;
            this.cells = new Cell[rows, cols];

            for (int x = 0; x < rows; x++) {
                for (int y = 0; y < cols; y++) {
                    Cell cell = new Cell(x, y);
                    this.cells[x, y] = cell;
                }
            }
        }

        private void createEdges() {
            int rows = this.rows;
            int cols = this.cols;

            for (int x = 0; x < rows; x++) {
                for (int y = 0; y < cols; y++) {
                    Cell cell = this.cells[x, y];
                    Cell adyCell;
                    string name;

                    if (x < this.rows - 1) {
                        adyCell = this.cells[x + 1, y];
                        name = cell.Name + "-" + adyCell.Name;
                        cell.South = new Edge(cell.vertex, adyCell.vertex, Util.rnd(), name);
                    }

                    if (y < this.cols - 1) {
                        adyCell = this.cells[x, y + 1];
                        name = cell.Name + "-" + adyCell.Name;
                        cell.East = new Edge(cell.vertex, adyCell.vertex, Util.rnd(), name);
                    }

                    if (x > 0) {
                        adyCell = this.cells[x - 1, y];
                        cell.North = adyCell.South;
                    }

                    if (y > 0) {
                        adyCell = this.cells[x, y - 1];
                        cell.West = adyCell.East;
                    }
                }
            }
        }

        private void createGraph() {
            int rows = this.rows;
            int cols = this.cols;

            List<Edge> edgeList = new List<Edge>();
            Kruskal.Kruskal kruskal = new Kruskal.Kruskal();

            for (int x = 0; x < rows; x++) {
                for (int y = 0; y < cols; y++) {
                    Cell cell = this.cells[x, y];

                    if (cell.South != null) { edgeList.Add(cell.South); }
                    if (cell.East  != null) { edgeList.Add(cell.East); }
                }
            }

            int total_cost;
            this.graph = kruskal.Solve(edgeList, out total_cost);

            foreach (Edge edge in this.graph) {
                edge.Check = true;
            }
        }

        private void markBorders() {

            int rows = this.rows;
            int cols = this.cols;

            Cell cell;
            for (int x = 0; x < rows; x++) {
                cell = this.cells[x, 0];
                if (cell.South != null) cell.South.Check = true;
                if (cell.North != null) cell.North.Check = true;
                cell = this.cells[x, cols - 1];
                if (cell.South != null) cell.South.Check = true;
                if (cell.North != null) cell.North.Check = true;
            }

            for (int y = 0; y < cols; y++) {
                cell = this.cells[0, y];
                if (cell.East != null) cell.East.Check = true;
                if (cell.West != null) cell.West.Check = true;
                cell = this.cells[rows - 1, y];
                if (cell.East != null) cell.East.Check = true;
                if (cell.West != null) cell.West.Check = true;
            }
        }

        private void joinFreeBorders() {
            int rows = this.rows;
            int cols = this.cols;

            Cell cell, ady;
            for (int x = 0; x < rows; x++) {
                for (int y = 0; y < cols - 1; y++) {
                    cell = this.cells[x, y];
                    ady  = this.cells[x, y + 1];

                    if (cell.Boundless && ady.Boundless) {
                        cell.East.Check = true;
                        ady.West.Check = true;
                    }
                }
            }

            for (int x = 0; x < rows - 1; x++) {
                for (int y = 0; y < cols; y++) {
                    cell = this.cells[x, y];
                    ady  = this.cells[x + 1, y];

                    if (cell.Boundless && ady.Boundless) {
                        cell.South.Check = true;
                        ady.North.Check = true;
                    }
                }
            }
        }


        public override string ToString() {
            string output = "";
            int rows = this.rows;
            int cols = this.cols;

            for (int x = 0; x < rows; x++) {
                for (int y = 0; y < cols; y++) {
                    output = output + this.cells[x, y].symbol();
                }
                output = output + "\n";
            }

            return output;
        }
    }
}


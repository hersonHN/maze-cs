using System;
using System.Collections.Generic;
using Kruskal;

namespace Maze {

    public class Cell {
        public int X, Y;
        public string name;
        public Edge North, South, West, East;
        public Vertex vertex;

        public bool HasNorth { 
            get {
                if (this.North == null) return false;
                return this.North.Check;
            }
        }

        public bool HasSouth { 
            get {
                if (this.South == null) return false;
                return this.South.Check;
            }
        }

        public bool HasWest { 
            get {
                if (this.West == null) return false;
                return this.West.Check;
            }
        }

        public bool HasEast { 
            get {
                if (this.East == null) return false;
                return this.East.Check;
            }
        }

        public bool Boundless {
            get {
                if ( this.HasNorth && !this.HasSouth && !this.HasEast && !this.HasWest) return true;
                if (!this.HasNorth &&  this.HasSouth && !this.HasEast && !this.HasWest) return true;
                if (!this.HasNorth && !this.HasSouth &&  this.HasEast && !this.HasWest) return true;
                if (!this.HasNorth && !this.HasSouth && !this.HasEast &&  this.HasWest) return true;
            
                return false;
            }

        }


        public string Name {
            get { return this.name; }
            set { this.name = value; }
        }

        public Cell(int x, int y) {
            this.X = x;
            this.Y = y;
            this.name = "X" + x + "Y" + y;
            Point location = new Point(x, y);
            this.vertex = new Vertex(this.name, location);
        }

        public string symbol() {
            bool north, south, west, east;

            north = this.HasNorth;
            south = this.HasSouth;
            west  = this.HasWest;
            east  = this.HasEast;

            if (!north && !south && !west && !east) return "   ";
            if (!north && !south && !west &&  east) return " ══";
            if (!north && !south &&  west && !east) return "══ ";
            if (!north && !south &&  west &&  east) return "═══";
            if (!north &&  south && !west && !east) return " ║ ";
            if (!north &&  south && !west &&  east) return " ╔═";
            if (!north &&  south &&  west && !east) return "═╗ ";
            if (!north &&  south &&  west &&  east) return "═╦═";
            if ( north && !south && !west && !east) return " ║ ";
            if ( north && !south && !west &&  east) return " ╚═";
            if ( north && !south &&  west && !east) return "═╝ ";
            if ( north && !south &&  west &&  east) return "═╩═";
            if ( north &&  south && !west && !east) return " ║ ";
            if ( north &&  south && !west &&  east) return " ╠═";
            if ( north &&  south &&  west && !east) return "═╣ ";
            if ( north &&  south &&  west &&  east) return "═╬═";

            return " ";
        }

    }
}



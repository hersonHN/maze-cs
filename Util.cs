
using System;

namespace Maze {
    public class Util {
        private static Random random = new Random();

        public static int rnd(float bias) {
            float num = random.Next(1, 10000) * bias;
            return (int) num;
        }

        public static int rnd() {
            return rnd(1f);
        }
    }
}
